
from distutils.core import setup  
from distutils.extension import Extension  
from Cython.Distutils import build_ext  
  
setup(  
  name = 'fdatec',
  ext_modules=[  
    Extension('fdatec', ['fdatec.pyx'])
    ],  
  cmdclass = {'build_ext': build_ext}  
)  