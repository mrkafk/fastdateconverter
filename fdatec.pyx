
import datetime
import pandas as pd
from libc.stdlib cimport atoi, malloc, free
from libc.string cimport strncpy, memset


def convert_date_fast(bytes pystr):
    """
    Format like: 2014/01/07 10:15:08

    Offsets in date string to parse:

    year:   0
    month:  5
    day:    8
    hour:   11
    minute: 14
    second: 17

    """
    cdef int d_year, d_month, d_day, d_hour, d_min, d_sec
    cdef char *date_str = <char *> malloc(19)
    cdef char *s
    s = pystr

    memset(date_str, 0, 19)
    strncpy(date_str, s+0, 4)
    d_year = atoi(date_str)

    memset(date_str, 0, 4)
    strncpy(date_str, s+5, 2)
    d_month = atoi(date_str)

    memset(date_str, 0, 2)
    strncpy(date_str, s+8, 2)
    d_day = atoi(date_str)

    memset(date_str, 0, 2)
    strncpy(date_str, s+11, 2)
    d_hour = atoi(date_str)

    memset(date_str, 0, 2)
    strncpy(date_str, s+14, 2)
    d_min = atoi(date_str)

    memset(date_str, 0, 2)
    strncpy(date_str, s+17, 2)
    d_sec = atoi(date_str)

    out = datetime.datetime(d_year, d_month, d_day, d_hour, d_min, d_sec)
    free(date_str)
    return pd.Timestamp(out)
