# README #

Fast string-to-date converter for Python

### What is this repository for? ###

* Autogeneration of Cython-based extension to quickly parse your particular date format
* v0.2
* Over 100 times faster than pandas.datetools.parse_time_string, see https://bitbucket.org/mrkafk/fastdateconverter/wiki/Home for example