#!/usr/bin/env python

import timeit

import distutils.core
import pandas as pd

cython_header = """
import datetime
import pandas as pd
from libc.stdlib cimport atoi, malloc, free
from libc.string cimport strncpy, memset
"""


class DateConverterException(Exception):
    pass


class FunDef(object):

    """ Date conversion function definition.

        function_name: this will be a function available as fdatec.function_name

        format_example: example date string (used for testing and comparison to pandas.datetools.parse_time_string result)

        *_offset: offset for particular datum in a converted string, e.g.
                  for "2012 01/01/01" offset of year is 0.
        *_len:    length of particular datum in a converted string, e.g.e
                  for ""2012 01/01/01" length of month is 2.
    """

    def __init__(self, function_name, format_example,
                 year_offset=-1, year_len=4,
                 month_offset=-1, month_len=2,
                 day_offset=-1, day_len=2,
                 hour_offset=-1, hour_len=2,
                 minute_offset=-1, minute_len=2,
                 second_offset=-1, second_len=2):
        self.kwargs = {'function_name': function_name, 'format_example': format_example.strip(),
              'year_offset': year_offset, 'year_len': year_len,
              'month_offset': month_offset, 'month_len': month_len,
              'day_offset': day_offset, 'day_len': day_len,
              'hour_offset': hour_offset, 'hour_len': hour_len,
              'minute_offset': minute_offset, 'minute_len': minute_len,
              'second_offset': second_offset, 'second_len': second_len}
        self.kwargs['date_str_len'] = len(format_example.strip())
        self.sanity_checks()

    def sanity_checks(self):
        format_example = self.kwargs['format_example']
        for num_name in ['year', 'month', 'day', 'hour', 'minute', 'second']:
            koffset, klen = '{0}_offset'.format(num_name), '{0}_len'.format(num_name)
            voffset, vlen = self.kwargs[koffset], self.kwargs[klen]
            if voffset == -1:
                raise DateConverterException('offset {0} cannot be default (-1), pls set it'.format(koffset))
            if voffset + vlen > len(format_example):
                raise DateConverterException('Incorrect parameters: offset {0} + length of {1} are greater than length of format example ({2})'.format(
                    koffset, klen, format_example))


class ConverterGenerator(object):

    """ Converter generator class.

        Pass a list of FunDef definitions to it.

    """

    def __init__(self, fundefs):
        self.fundefs = fundefs
        self.generated_names = []
        self.generate_cython_code()
        self.generate_extension()
        self.test_extension()

    def generate_cython_code(self):
        cython_code = [cython_header]
        cython_tmpl = ''.join(open('cython_template.txt').readlines())
        for fdef in self.fundefs:
            fstr = cython_tmpl.format(**fdef.kwargs)
            cython_code.append(fstr)
        with open('fdatec.pyx', 'wb') as fo:
            fo.write('\n'.join(cython_code))

    def generate_extension(self):
        c = distutils.core.run_setup('setup.py', ['clean', 'install'])
        c.run_commands()

    def test_extension(self):
        try:
            import fdatec
        except ImportError:
            pass
        reload(fdatec)
        for fdef in self.fundefs:
            fun_name = fdef.kwargs['function_name']
            fun = getattr(fdatec, fun_name)
            format_example = fdef.kwargs['format_example']
            cdate = fun(format_example)
            t = pd.datetools.parse_time_string(format_example)[0]
            ts = pd.Timestamp(t)
            assert cdate == ts, 'Warning: generated converter produces a different result ({0}) than pandas.datetools.parse_time_string ({1})!'.format(cdate, ts)
            self.generated_names.append((fun_name, format_example))

    def benchmark(self):
        for fun_name, format_example in self.generated_names:
            stmt = """{0}('{1}')""".format(fun_name, format_example)
            stmt_setup = 'from fdatec import {0}'.format(fun_name)
            num = 10000
            print 'Benchmarking {0}'.format(stmt)
            t = timeit.timeit(stmt, stmt_setup, number=num)
            t1 = t/num
            print 'Elapsed time: {0}s, runs: {1}, ({2}s / run)'.format(t, num, t1)
            s2 = """parse_time_string('{0}')""".format(format_example)
            s2_setup = "import pandas; parse_time_string = pandas.datetools.parse_time_string"
            print 'Benchmarking {0}'.format(s2)
            t = timeit.timeit(s2, s2_setup, number=num)
            t2 = t/num
            print 'Elapsed time: {0}s, runs: {1}, ({2}s / run)'.format(t, num, t2)
            print 'Ratio pandas.datetools.parse_time_string time / generated converter time: {0}'.format(t2/t1)


if __name__ == '__main__':
    fdef1 = FunDef('convert_date_fast', '2014/01/07 10:15:08', year_offset=0,
                   month_offset=5, day_offset=8, hour_offset=11, minute_offset=14, second_offset=17)
    cg = ConverterGenerator([fdef1])
    cg.benchmark()
